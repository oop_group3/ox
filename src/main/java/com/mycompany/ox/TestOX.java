/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ox;

import java.util.Scanner;

/**
 *
 * @author Windows10
 */
public class TestOX {

    public static void main(String[] args) {
        OX ox = new OX();
        char[][] board = new char[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
                System.out.print(" ");
            }
        }

        Scanner kb = new Scanner(System.in);
        boolean play = true;
        boolean end = false;
        int row = 0;
        int col = 0;
        char x;
        System.out.println("Welcome to OX Game");
        while (!end) {
            ox.CreateBoard(board);
            if (play) {
                System.out.println("Turn O");
            } else {
                System.out.println("Turn X");
            }
            if (play) {
                x = 'o';
            } else {
                x = 'x';
            }
            while (true) {

                System.out.println("Please input row, col: ");
                row = kb.nextInt();
                col = kb.nextInt();
                System.out.println();
                row = row - 1;
                col = col - 1;

                if (row < 0 || col < 0 || row > 2 || col > 2) {
                    System.out.println("Error: Out of Bound");

                } else if (board[row][col] != '-') {
                    System.out.println("This position is already taken");

                } else {
                    break;
                }
            }
            board[row][col] = x;

            if (ox.Winnable(board) == 'x') {
                ox.CreateBoard(board);
                System.out.println("");
                System.out.println(">>>X Win<<<");
                end = true;
            } else if (ox.Winnable(board) == 'o') {
                ox.CreateBoard(board);
                System.out.println("");
                System.out.println(">>>O Win<<<");
                end = true;
            } else {
                if (ox.Full(board)) {
                    ox.CreateBoard(board);
                    System.out.println("");
                    System.out.println(">>>Draw<<<");
                    end = true;
                } else {
                    play = !play;
                }
            }
        }

    }

}
